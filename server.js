const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3003;
const route = require('./route');
const expressSwagger = require('express-swagger-generator')(app);

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());

route(app);

let options = {
    swaggerDefinition: {
        info: {
            description: 'This is a sample server',
            title: 'Swagger',
            version: '1.0.0',
        },
        host: 'localhost:3003',
        basePath: '/v1',
        produces: [
            "application/json",
            "application/xml"
        ],
        schemes: ['http', 'https'],
        securityDefinitions: {
            JWT: {
                type: 'apiKey',
                in: 'header',
                name: 'Authorization',
                description: "",
            }
        }
    },
    basedir: __dirname, //app absolute path
    files: ['./route.js'] //Path to the API handle folder
};
expressSwagger(options);

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})
