const db = require('./query');
const express = require('express');
const app = express();

const InitializeRoute = (app)=>{
    /**
     * This function comment is parsed by doctrine
     * @route GET /organizations/Campus
     * @group foo - Users information
     * @returns {object} 200 - An array of user info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/organizations/Campus', db.getCampus);
    /**
     * This function comment is parsed by doctrine
     * @route GET /organizations/School
     * @group foo - User information
     * @returns {object} 200 - A user info
     * @returns {Error}  default - Unexpected error
     */
    app.get('/organizations/School/:id', db.getSchoolByCampus);
  
  
}
module.exports= InitializeRoute;
